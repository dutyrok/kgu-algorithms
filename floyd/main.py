#!/usr/bin/python3

import click
import logging
import traceback


class GraphPath:
    @classmethod
    def create_empty(cls) -> "GraphPath":
        return cls("", "", empty=True)

    def __init__(
        self, start: str, target: str, *transitive, empty: bool = False
    ):
        self._start = start
        self._target = target
        self._transitive = transitive
        self._empty = empty

    @property
    def is_empty(self):
        return self._empty

    def __add__(self, other: "GraphPath") -> "GraphPath":
        if self.is_empty or other.is_empty:
            raise Exception("Operands must be not empty")
        return GraphPath(
            self._start, other._target,
            *self._transitive, self._target, *other._transitive
        )

    def __str__(self):
        if len(self._transitive) != 0:
            return f"{self._start}-{'-'.join(self._transitive)}-{self._target}"
        if self.is_empty:
            return "NO"
        return f"{self._start}-{self._target}"


def fill_in_automatically_paths(
    weight_matrix: list[list[float]]
) -> list[list[GraphPath]]:
    result: list[list[GraphPath]] = []
    for row in range(len(weight_matrix)):
        result.append([])
        for column in range(len(weight_matrix)):
            if row == column or weight_matrix[row][column] == float("inf"):
                result[row].append(GraphPath.create_empty())
                continue
            result[row].append(GraphPath(str(row+1), str(column+1)))
    return result


def print_matrix(matrix: list[list], end=""):
    for row in range(len(matrix)):
        for column in range(len(matrix)):
            if row == column:
                value = "--"
            else:
                value = str(matrix[row][column])
            print(value, end=" ")
        print()
    print(end=end)


def decide_floyd(
    weight_matrix: list[list[float]],
    path_matrix: list[list[GraphPath]],
    short=False
):
    size = len(weight_matrix)
    for base in range(size):
        logging.info(f"base: {base+1}")
        for row in range(size):
            if row == base:
                continue
            if weight_matrix[row][base] == float("inf") and short:
                logging.debug(
                    f"row '{row+1}' from base column {base+1} is INF, skip..."
                )
                continue
            for column in range(size):
                if (
                    column == base
                    or column == row
                ):
                    continue

                if weight_matrix[base][column] == float("inf") and short:
                    logging.debug(
                        f"[{row+1}:{column+1}]: column '{column+1}' from "
                        + f"base row {base+1} is INF, skip..."
                    )
                    continue

                old_value = weight_matrix[row][column]
                cross_amount = \
                    weight_matrix[row][base] + weight_matrix[base][column]
                old_value = weight_matrix[row][column]
                weight_matrix[row][column] = min(
                    weight_matrix[row][column], cross_amount
                )
                if old_value != weight_matrix[row][column]:
                    path_matrix[row][column] = \
                        path_matrix[row][base] + path_matrix[base][column]
                logging.info(
                    f"[{row+1}:{column+1}] = min({old_value}, "
                    + f"{cross_amount}) = {weight_matrix[row][column]}; "
                    + f"path is {path_matrix[row][column]}"
                )


def get_weight_matrix(matrix_file: str) -> list[list[float]]:
    result: list[list[float]] = []
    with open(matrix_file, "r") as fd:
        file_data = [line.replace("\n", "") for line in fd.readlines()]
    logging.debug(file_data)
    size = len(file_data)
    logging.debug(f"matrix_size: {size}")
    if size == 0:
        raise Exception("Matrix can have size equals 0")

    for row in range(size):
        result_row: list[float] = []
        splited = file_data[row].split(" ")
        if len(splited) != size:
            raise Exception(
                f"in row is '{row+1}' got {len(splited)}, expected {size}"
            )
        for column in range(size):
            try:
                if column == row:
                    if splited[column] != "-":
                        raise Exception(
                            "elements from main diagonale must be '-'"
                        )
                    else:
                        result_row.append(0.0)
                else:
                    if splited[column] == "-":
                        raise Exception(
                            "elements not from main diagonale must be not '-'"
                        )
                    elif splited[column] == "":
                        result_row.append(float("inf"))
                    else:
                        result_row.append(float(splited[column]))
                        if result_row[column] == 0.0:
                            raise Exception("weight can't be equal 0")
            except Exception as err:
                raise Exception(f"at [{row+1}:{column+1}]: {err}")

        result.append(result_row)
    logging.debug("parsed matrix: " + str(result))
    return result


@click.command(
    help="Decide Floyd algorithm",
    context_settings={
        "help_option_names": ['-h', '--help']
    }
)
@click.option(
    "-v", "--verbose", is_flag=True, default=False,
    help="Show debug type message"
)
@click.option(
    "-f", "--matrix_file", type=click.Path(exists=True), required=True,
    help="Text file with matrix"
)
@click.option(
    "-s", "--short", is_flag=True, default=False,
    help="Skip base columns and rows are equals infinity"
)
def cli(verbose, matrix_file, short):
    log_level = logging.INFO
    if verbose:
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level, format="[%(levelname)s]: %(message)s")

    try:
        weight_matrix = get_weight_matrix(matrix_file)
        path_matrix = fill_in_automatically_paths(weight_matrix)

        decide_floyd(weight_matrix, path_matrix, short)

        print("--Result--\nWeights:")
        print_matrix(weight_matrix, end="\n")
        print("Paths:")
        print_matrix(path_matrix)
    except Exception as err:
        logging.critical(err)
        logging.debug("".join(traceback.format_tb(err.__traceback__)))


if __name__ == "__main__":
    cli()
