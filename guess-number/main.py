#!/usr/bin/python3

import random


class NumberIsGuessed(Exception):
    pass


class InputNotIsNumber(Exception):
    pass


class NotInRange(Exception):
    pass


def print_difference_between_numbers(guess: int, user: int):
    if user < guess:
        print("Your number is less than guessed one")
    else:
        print("Your number is greater than guessed one")


def do_attemp(attemp: int, guessed_number: int):
    try:
        user_number_str = input(
            f"[{attemp}]: Please enter a number between 0 and 1000: "
        )
        user_number = int(user_number_str)
    except ValueError:
        print(f"'{user_number_str}' isn not a number. Try again...")
        raise InputNotIsNumber()
    if user_number < 0 or user_number > 1000:
        print("Your number must be in range from 0 to 1000")
        raise NotInRange()
    if guessed_number == user_number:
        raise NumberIsGuessed()
    print_difference_between_numbers(guessed_number, user_number)


def main():
    guessed_number = random.randint(0, 1001)
    attemps = 10
    while attemps > 0:
        try:
            do_attemp(11 - attemps, guessed_number)
        except (InputNotIsNumber, NotInRange):
            continue
        except NumberIsGuessed:
            break
        attemps -= 1
    else:
        print(f"It was '{guessed_number}'. You LOSE...")
        return
    print("You WIN. My congratulations!")


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("\nProgram was interrupted by user...")
        exit(1)
    except Exception as e:
        print("ERR:", e)
        exit(2)
