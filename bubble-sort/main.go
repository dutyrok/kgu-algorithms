package main

import "fmt"

func bubbleSort(arr *[]int) {
	lastAvailableIndex := len(*arr) - 1
	for i := 0; i < len(*arr)-1; i++ {
		isSwap := false
		for j := 0; j < lastAvailableIndex; j++ {
			if (*arr)[j] > (*arr)[j+1] {
				if !isSwap {
					isSwap = true
				}
				temp := (*arr)[j]
				(*arr)[j] = (*arr)[j+1]
				(*arr)[j+1] = temp
			}
		}
		if !isSwap {
			fmt.Println("Breaked")
			break
		}
		lastAvailableIndex--
		fmt.Println(fmt.Sprintf("Iter(%d):", i+1), *arr)
	}

}

func sortArray(arr []int) {
	fmt.Println("Source:", arr)
	bubbleSort(&arr)
	fmt.Println("Sorted:", arr)
}

func main() {
	var arrSet = [][]int{
		{8, 6, 9, 6, 3, 1, 9, 4},
		{9, 8, 7, 6, 5, 4, 3, 2, 1},
	}
	for _, arr := range arrSet {
		sortArray(arr)
		fmt.Println()
	}
}
