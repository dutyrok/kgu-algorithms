package main

import "fmt"

type minimalElem struct {
	value int
	index int
}

func selectionSort(arr *[]int) {
	for i := 0; i < len(*arr)-1; i++ {
		min := minimalElem{value: (*arr)[i], index: i}
		for j := i + 1; j < len(*arr); j++ {
			if (*arr)[j] < min.value {
				min = minimalElem{value: (*arr)[j], index: j}
			}
		}
		if min.value != (*arr)[i] {
			(*arr)[min.index] = (*arr)[i]
			(*arr)[i] = min.value
		}
		fmt.Println(fmt.Sprintf("Iter(%d):", i+1), *arr)
	}
}

func sortArray(arr []int) {
	fmt.Println("Source:", arr)
	selectionSort(&arr)
	fmt.Println("Sorted:", arr)
}

func main() {
	var arrSet = [][]int{
		{8, 6, 9, 6, 3, 1, 9, 4},
		{9, 8, 7, 6, 5, 4, 3, 2, 1},
	}
	for _, arr := range arrSet {
		sortArray(arr)
		fmt.Println()
	}
}
