package main

import "fmt"

func inclusionSort(arr *[]int) {
	for i := 1; i < len(*arr); i++ {
		value := (*arr)[i]
		index := i
		for index > 0 && (*arr)[index-1] > value {
			(*arr)[index] = (*arr)[index-1]
			index--
		}
		(*arr)[index] = value
		fmt.Println(fmt.Sprintf("Iter(%d):", i), *arr)
	}
}

func sortArray(arr []int) {
	fmt.Println("Source:", arr)
	inclusionSort(&arr)
	fmt.Println("Sorted:", arr)
}

func main() {
	var arrSet = [][]int{
		{8, 6, 9, 6, 3, 1, 9, 4},
		{9, 8, 7, 6, 5, 4, 3, 2, 1},
	}
	for _, arr := range arrSet {
		sortArray(arr)
		fmt.Println()
	}
}
