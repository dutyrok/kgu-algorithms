#include "tree.h"
#include <stdlib.h>

tree *create_node(int value, tree *left, tree *rigth){
    tree *temp = malloc(sizeof(tree));
    temp->value = value;
    temp->left = left;
    temp->right = rigth;
    return temp;
}

void free_tree_mem(tree *node){
    if(node == NULL){
        return;
    }
    free_tree_mem(node->left);
    free_tree_mem(node->right);
    free(node);
}
