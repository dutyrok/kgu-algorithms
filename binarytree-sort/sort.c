#include <stdio.h>
#include <stdlib.h>
#include "tree.h"


void add_node(tree *node, int value){
    if(value < node->value){
        if(node->left != NULL){
            add_node(node->left, value);
        }
        else {
            node->left = create_node(value, NULL, NULL);
        }
    } 
    else {
        if (node->right != NULL){
            add_node(node->right, value);
        }
        else {
            node->right = create_node(value, NULL, NULL);
        }
    }
}

tree *arr_to_binary_tree(tree *node, int *arr, int av_size){
    if(!av_size){
        return NULL;
    }
    if(node == NULL){
        node = create_node(*arr, NULL, NULL);
    }
    else{
        add_node(node, *arr);
    }    
    arr_to_binary_tree(node, arr+1, av_size-1);
    return node;
}

void add_to_arr(tree *node, int *arr, int *i){
    // In-order way of binary tree traversal
    if(node == NULL){
        return;
    }
    add_to_arr(node->left, arr, i);
    arr[*i] = node->value;
    ++*i;
    add_to_arr(node->right, arr, i);
}

int *sort_by_binary_tree(int *arr, int arr_size){
    int *result = malloc(sizeof(*arr) * arr_size);
    int first_index = 0;
    tree *first_node = arr_to_binary_tree(NULL, arr, arr_size);
    add_to_arr(first_node, result, &first_index);
    free_tree_mem(first_node);
    return result;
}

void print_arr(int *arr, int arr_size){
    for(int i = 0; i < arr_size; i++){
        if(!i){
            printf("%d", arr[i]);
        }
        else{
            printf(", %d", arr[i]);
        }
    }
    printf("\n");
}

int main(){
    int arr[10] = {8, 6, 9, 21, 3, 1, 14, 4, 12, 10};
    int *sort_arr = sort_by_binary_tree(arr, sizeof(arr) / sizeof(arr[0]));
    print_arr(sort_arr, sizeof(arr) / sizeof(arr[0]));
    return EXIT_SUCCESS;
}