typedef struct tag_tree{
    int value;
    struct tag_tree *left, *right;
} tree;

tree *create_node(int value, tree *left, tree *rigth);
void free_tree_mem(tree *node);