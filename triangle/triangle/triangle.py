from dataclasses import dataclass
from enum import Enum, auto
import math

PRECISION = 14


class SideLength(float):
    def round(self) -> float:
        return round(self, PRECISION)


@dataclass(frozen=True)
class Point:
    x: float
    y: float

    def side_length(self, other: "Point") -> SideLength:
        x_catet = abs(self.x - other.x)
        y_catet = abs(self.y - other.y)
        return SideLength(math.sqrt(x_catet ** 2 + y_catet ** 2))


class Triangle:
    def __init__(self, a: Point, b: Point, c: Point):
        self._ab = a.side_length(b)
        self._ac = a.side_length(c)
        self._bc = b.side_length(c)

        self._side_groups = (
            (self._ab, self._ac, self._bc),
            (self._ac, self._bc, self._ab),
            (self._bc, self._ab, self._ac)
        )

    def is_existing(self) -> bool:
        def is_not_smaller(
            side1: SideLength, side2: SideLength, side3: SideLength
        ) -> bool:
            return not side1 < side2 + side3

        for group in self._side_groups:
            if is_not_smaller(*group):
                return False
        return True

    def is_right_angle(self) -> bool:
        def is_piphagor_theorem(
            side1: SideLength, side2: SideLength, side3: SideLength
        ) -> bool:
            return round(side1 ** 2, PRECISION) == side2 ** 2 + side3 ** 2

        for group in self._side_groups:
            if is_piphagor_theorem(*group):
                return True
        return False

    def is_isosceles(self) -> bool:
        def are_two_sides_equal(
            side1: SideLength, side2: SideLength, *_
        ) -> bool:
            return side1.round() == side2.round()

        for group in self._side_groups:
            if are_two_sides_equal(*group):
                return True
        return False

    def is_equilateral(self) -> bool:
        def are_all_sides_equal(
            side1: SideLength, side2: SideLength, side3: SideLength
        ) -> bool:
            return side1.round() == side2.round() == side3.round()

        for group in self._side_groups:
            if are_all_sides_equal(*group):
                return True
        return False


class TriangleType(Enum):
    not_existing = auto()
    right_angle = auto()
    isosceles = auto()
    right_angle_and_isosceles = auto()
    equilateral = auto()
    versatile = auto()


def determine_triangle(a: Point, b: Point, c: Point) -> TriangleType:
    triangle = Triangle(a, b, c)
    if not triangle.is_existing():
        return TriangleType.not_existing
    if triangle.is_right_angle():
        if triangle.is_isosceles():
            return TriangleType.right_angle_and_isosceles
        return TriangleType.right_angle
    if triangle.is_equilateral():
        return TriangleType.equilateral
    if triangle.is_isosceles():
        return TriangleType.isosceles
    return TriangleType.versatile
