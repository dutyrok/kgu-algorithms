import click
import logging
import re
import traceback

from .triangle import determine_triangle, Point, TriangleType

_FLOAT_PATTERN = r'([+-]?\d+(\.\d*)?|\.\d+)'
_POINT_PATTERN = _FLOAT_PATTERN + r':' + _FLOAT_PATTERN


class ClickPoint(click.ParamType):
    def convert(self, value, param, ctx):
        if isinstance(value, Point):
            return value

        result = re.fullmatch(_POINT_PATTERN, value)
        if not result:
            raise click.BadParameter("Point must be '%f:%f'")
        return Point(float(result.groups()[0]), float(result.groups()[2]))

    @property
    def name(self):
        return "point"


TRIANGLE_TYPE_TO_STR = {
    TriangleType.not_existing: "Your triangle doesn't exist",
    TriangleType.right_angle: "You triangle is right one",
    TriangleType.isosceles: "Your triangle is isosceles one",
    TriangleType.right_angle_and_isosceles:
        "Your triangle is right and isosceles one",
    TriangleType.equilateral: "Your triangle is equilateral one",
    TriangleType.versatile: "Your triangle is versatile one"
}


@click.command(
    help="Determining type of specified Triangle"
)
@click.option(
    "-a", type=ClickPoint(), prompt=True, default="0:0",
    help="The 'A' point of triangle"
)
@click.option(
    "-b", type=ClickPoint(), prompt=True, default="0:0",
    help="The 'B' point of triangle"
)
@click.option(
    "-c", type=ClickPoint(), prompt=True, default="0:0",
    help="The 'C' point of triangle"
)
@click.option(
    "-v", "--verbose", is_flag=True, default=False,
    help="Show debug type message"
)
def cli(a, b, c, verbose):
    log_level = logging.INFO
    if verbose:
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level, format="[%(levelname)s]: %(message)s")

    try:
        print(TRIANGLE_TYPE_TO_STR[determine_triangle(a, b, c)])
    except Exception as err:
        logging.critical(err)
        logging.debug("".join(traceback.format_tb(err.__traceback__)))
