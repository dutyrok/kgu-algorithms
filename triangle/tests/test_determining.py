import pytest
from typing import NamedTuple
import math

from triangle.triangle import TriangleType, Point, determine_triangle


class Parameter(NamedTuple):
    points: tuple[Point, Point, Point]
    expected: TriangleType


parameters = [
    Parameter(
        (Point(0, 0), Point(0, 1), Point(0, 2)),
        TriangleType.not_existing
    ),
    # special case, when all points are 0:0
    Parameter(
        (Point(0, 0), Point(0, 0), Point(0, 0)),
        TriangleType.not_existing
    ),
    Parameter(
        (Point(0, 0), Point(0, 1), Point(1, 0)),
        TriangleType.right_angle_and_isosceles
    ),
    Parameter(
        (Point(0, 0), Point(0, 1), Point(3.6, 0)),
        TriangleType.right_angle
    ),
    Parameter(
        (Point(0, 0), Point(2, 0), Point(1, math.sqrt(3))),
        TriangleType.equilateral
    ),
    Parameter(
        (Point(2, 0), Point(0, 7), Point(4, 7)),
        TriangleType.isosceles
    ),
    Parameter(
        (Point(0, 0), Point(0, 1), Point(1, 2)),
        TriangleType.versatile
    ),
    # Check precision of point coordinate
    Parameter(
        (Point(0, 0), Point(0, 1), Point(0.000001, 2)),
        TriangleType.versatile
    ),
]


@pytest.mark.parametrize('parameter', parameters)
def test_triangle_determining(parameter: Parameter):
    assert determine_triangle(*parameter.points) is parameter.expected
